function pairProgramming(experiences, isMostExperienced) {
  const result = [];
  if (isMostExperienced) {
    const max = Math.max.apply(null, experiences);
    const firstIndex = experiences.indexOf(max)
    result.push(firstIndex)
    experiences.splice(experiences.indexOf(max), 1);
    const second = Math.max.apply(null, experiences);
    const secondIndex = experiences.indexOf(second);
    result.push(secondIndex > firstIndex ? secondIndex + 1 : secondIndex)
  } else {
    const min = Math.min.apply(null, experiences);
    const firstIndex = experiences.indexOf(min)
    result.push(firstIndex)
    experiences.splice(experiences.indexOf(min), 1);
    const second = Math.min.apply(null, experiences);
    const secondIndex = experiences.indexOf(second)
    result.push(secondIndex > firstIndex ? secondIndex + 1 : secondIndex)
  }
  return result;
}


console.log(pairProgramming([1, 4, 3, 2, 8, 3], true));
console.log(pairProgramming([1, 4, 3, 2, 8, 3], false));
console.log(pairProgramming([0, 3, 1, 6, 2, 3, 3, 6], true));