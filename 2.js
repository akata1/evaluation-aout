function nonConsecutiveDigits(n) {
  var j = n + 1
  while (j.toString().match(/(00|11|22|33|44|55|66|77|88|99)/)) {
    j++;
  }
  return j;
}

console.log(nonConsecutiveDigits(7));
console.log(nonConsecutiveDigits(1765));
console.log(nonConsecutiveDigits(44432));